package com.testng.browsers.pkg;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TestFirefoxChrome {

	String baseUrl = "http://demo.guru99.com/selenium/newtours/";
	
	WebDriver driver ;

	@Parameters({"Browser"})
	@BeforeTest
	public void launchBrowser(String browserType)
	{
		if(browserType.equalsIgnoreCase("Firefox"))
		{
			//String driverPath = ".\\FirefoxbrowserDriver\\geckodriver.exe";
			//System.setProperty("webdriver.firefox.marionette", driverPath);
			driver=new FirefoxDriver();
		}
		else if(browserType.equalsIgnoreCase("chrome"))
		{
			String driverPath = ".\\ChromeDriver\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", driverPath);
			driver=new ChromeDriver();
		}
		
		driver.manage().window().maximize();
		driver.get(baseUrl);

	}

	@BeforeMethod
	public void testBeforeMethodExecution()
	{
		System.out.println("Running Before Method");
	}
	
	@AfterMethod
	public void testAfterMethodExecution()
	{
		System.out.println("Running After Method");
	}
	
	@Parameters({"pageTitle"})
	@Test
	public void verifyHomepageTitle(String expectedTitle) 
	{
		//String expectedTitle = "Welcome: Mercury Tours";
		System.out.println("Running Verify Home Page Title");
		String actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, expectedTitle);
	}
	@Test
	public void reTest()
	{
		System.out.println("Running retest Method");
	}

	@AfterTest
	public void terminateBrowser()
	{
		driver.close();
	}
}
